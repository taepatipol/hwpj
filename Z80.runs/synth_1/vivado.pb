
w
Command: %s
53*	vivadotcl2F
2synth_design -top z80_tester -part xc7a35tcpg236-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7a35t2default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7a35t2default:defaultZ17-349h px� 
�
,redeclaration of ansi port %s is not allowed2611*oasys2
clkDiv2default:default2f
PD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/clockDiv.v2default:default2
82default:default8@Z8-2611h px� 
�
,redeclaration of ansi port %s is not allowed2611*oasys2
vsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
542default:default8@Z8-2611h px� 
�
,redeclaration of ansi port %s is not allowed2611*oasys2
hsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
542default:default8@Z8-2611h px� 
�
%s has already been declared976*oasys2
vsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
542default:default8@Z8-976h px� 
�
 second declaration of %s ignored2654*oasys2
vsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
542default:default8@Z8-2654h px� 
�
%s is declared here994*oasys2
vsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
132default:default8@Z8-994h px� 
�
%s is declared here994*oasys2
hsync2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
132default:default8@Z8-994h px� 
�
,redeclaration of ansi port %s is not allowed2611*oasys2
segments2default:default2l
VD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/segmentDecoder.v2default:default2
82default:default8@Z8-2611h px� 
�
%s*synth2�
xStarting RTL Elaboration : Time (s): cpu = 00:00:03 ; elapsed = 00:00:05 . Memory (MB): peak = 392.563 ; gain = 114.234
2default:defaulth px� 
�
synthesizing module '%s'%s4497*oasys2

z80_tester2default:default2
 2default:default2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
clockDiv2default:default2
 2default:default2f
PD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/clockDiv.v2default:default2
32default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
clockDiv2default:default2
 2default:default2
12default:default2
12default:default2f
PD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/clockDiv.v2default:default2
32default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2$
z80_top_direct_n2default:default2
 2default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/z80_top_direct_n.v2default:default2
192default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	clk_delay2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/clk_delay.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	clk_delay2default:default2
 2default:default2
22default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/clk_delay.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
decode_state2default:default2
 2default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/decode_state.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
decode_state2default:default2
 2default:default2
32default:default2
12default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/decode_state.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
execute2default:default2
 2default:default2[
ED:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/execute.v2default:default2
382default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
execute2default:default2
 2default:default2
42default:default2
12default:default2[
ED:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/execute.v2default:default2
382default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

interrupts2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/interrupts.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

interrupts2default:default2
 2default:default2
52default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/interrupts.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
ir2default:default2
 2default:default2V
@D:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/ir.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
ir2default:default2
 2default:default2
62default:default2
12default:default2V
@D:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/ir.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
pin_control2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/pin_control.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
pin_control2default:default2
 2default:default2
72default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/pin_control.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

pla_decode2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/pla_decode.v2default:default2
172default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

pla_decode2default:default2
 2default:default2
82default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/pla_decode.v2default:default2
172default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
resets2default:default2
 2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/resets.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
resets2default:default2
 2default:default2
92default:default2
12default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/resets.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

memory_ifc2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/memory_ifc.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

memory_ifc2default:default2
 2default:default2
102default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/memory_ifc.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
	sequencer2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/sequencer.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	sequencer2default:default2
 2default:default2
112default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/sequencer.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
alu_control2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_control.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	alu_mux_42default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_4.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	alu_mux_42default:default2
 2default:default2
122default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_4.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
	alu_mux_82default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_8.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	alu_mux_82default:default2
 2default:default2
132default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_8.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
alu_control2default:default2
 2default:default2
142default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_control.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

alu_select2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_select.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

alu_select2default:default2
 2default:default2
152default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_select.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
	alu_flags2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_flags.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	alu_mux_22default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_2.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	alu_mux_22default:default2
 2default:default2
162default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_2.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	alu_flags2default:default2
 2default:default2
172default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_flags.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
alu2default:default2
 2default:default2W
AD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
alu_core2default:default2
 2default:default2\
FD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_core.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	alu_slice2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_slice.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	alu_slice2default:default2
 2default:default2
182default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_slice.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
alu_core2default:default2
 2default:default2
192default:default2
12default:default2\
FD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_core.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2"
alu_bit_select2default:default2
 2default:default2b
LD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_bit_select.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
alu_bit_select2default:default2
 2default:default2
202default:default2
12default:default2b
LD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_bit_select.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2$
alu_shifter_core2default:default2
 2default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_shifter_core.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2$
alu_shifter_core2default:default2
 2default:default2
212default:default2
12default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_shifter_core.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

alu_mux_2z2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_2z.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

alu_mux_2z2default:default2
 2default:default2
222default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_2z.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

alu_mux_3z2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_3z.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

alu_mux_3z2default:default2
 2default:default2
232default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_mux_3z.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
alu_prep_daa2default:default2
 2default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_prep_daa.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
alu_prep_daa2default:default2
 2default:default2
242default:default2
12default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu_prep_daa.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
alu2default:default2
 2default:default2
252default:default2
12default:default2W
AD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/alu.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
reg_file2default:default2
 2default:default2\
FD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_file.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	reg_latch2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_latch.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	reg_latch2default:default2
 2default:default2
262default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_latch.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
reg_file2default:default2
 2default:default2
272default:default2
12default:default2\
FD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_file.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
reg_control2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_control.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
reg_control2default:default2
 2default:default2
282default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/reg_control.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2!
address_latch2default:default2
 2default:default2a
KD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_latch.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
address_mux2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_mux.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
address_mux2default:default2
 2default:default2
292default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_mux.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
inc_dec2default:default2
 2default:default2[
ED:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/inc_dec.v2default:default2
322default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2 
inc_dec_2bit2default:default2
 2default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/inc_dec_2bit.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
inc_dec_2bit2default:default2
 2default:default2
302default:default2
12default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/inc_dec_2bit.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
inc_dec2default:default2
 2default:default2
312default:default2
12default:default2[
ED:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/inc_dec.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
address_latch2default:default2
 2default:default2
322default:default2
12default:default2a
KD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_latch.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
bus_control2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/bus_control.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
bus_control2default:default2
 2default:default2
332default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/bus_control.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2

bus_switch2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/bus_switch.v2default:default2
252default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

bus_switch2default:default2
 2default:default2
342default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/bus_switch.v2default:default2
252default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
data_switch2default:default2
 2default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_switch.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
data_switch2default:default2
 2default:default2
352default:default2
12default:default2_
ID:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_switch.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2$
data_switch_mask2default:default2
 2default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_switch_mask.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2$
data_switch_mask2default:default2
 2default:default2
362default:default2
12default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_switch_mask.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
address_pins2default:default2
 2default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_pins.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
address_pins2default:default2
 2default:default2
372default:default2
12default:default2`
JD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/address_pins.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
	data_pins2default:default2
 2default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_pins.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	data_pins2default:default2
 2default:default2
382default:default2
12default:default2]
GD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/data_pins.v2default:default2
322default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2"
control_pins_n2default:default2
 2default:default2b
LD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/control_pins_n.v2default:default2
322default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2"
control_pins_n2default:default2
 2default:default2
392default:default2
12default:default2b
LD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/control_pins_n.v2default:default2
322default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2$
z80_top_direct_n2default:default2
 2default:default2
402default:default2
12default:default2d
ND:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/x/z80_top_direct_n.v2default:default2
192default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2'
mem_mapped_keyboard2default:default2
 2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
32default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter DATA_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter ADDR_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
�
,$readmem data file '%s' is read successfully3426*oasys2
	data2.mem2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1762default:default8@Z8-3876h px� 
�
,$readmem data file '%s' is read successfully3426*oasys2
data.mem2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1772default:default8@Z8-3876h px� 
�
synthesizing module '%s'%s4497*oasys2
vga_sync2default:default2
 2default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/vga_sync.v2default:default2
12default:default8@Z8-6157h px� 
`
%s
*synth2H
4	Parameter H_DISPLAY bound to: 640 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter H_L_BORDER bound to: 48 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter H_R_BORDER bound to: 16 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter H_RETRACE bound to: 96 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter H_MAX bound to: 799 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter START_H_RETRACE bound to: 656 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter END_H_RETRACE bound to: 751 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter V_DISPLAY bound to: 480 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter V_T_BORDER bound to: 10 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter V_B_BORDER bound to: 33 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter V_RETRACE bound to: 2 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter V_MAX bound to: 524 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter START_V_RETRACE bound to: 513 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter END_V_RETRACE bound to: 514 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
vga_sync2default:default2
 2default:default2
412default:default2
12default:default2^
HD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/vga_sync.v2default:default2
12default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
blackOrWhite2default:default2
 2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/blackOrWhite.v2default:default2
232default:default8@Z8-6157h px� 
^
%s
*synth2F
2	Parameter BALL_SIZE bound to: 8 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter BAR_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter BAR_HEIGHT bound to: 100 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter scorePlayer1X bound to: 200 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter scorePlayer1Y bound to: 30 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter scorePlayer2X bound to: 400 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter scorePlayer2Y bound to: 30 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter scoreWidth bound to: 16 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter scoreHeight bound to: 20 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter groupNameRow bound to: 100 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter memberRow0 bound to: 200 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter memberRow1 bound to: 300 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter memberRow2 bound to: 400 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter memberColumnStart bound to: 200 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter bar1X bound to: 100 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter bar2X bound to: 532 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'%s4497*oasys2 
whichSegment2default:default2
 2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/whichSegment.v2default:default2
232default:default8@Z8-6157h px� 
X
%s
*synth2@
,	Parameter segmentSize bound to: 6'b000100 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
whichSegment2default:default2
 2default:default2
422default:default2
12default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/whichSegment.v2default:default2
232default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2 
pixelSegment2default:default2
 2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/pixelSegment.v2default:default2
232default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
pixelSegment2default:default2
 2default:default2
432default:default2
12default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/pixelSegment.v2default:default2
232default:default8@Z8-6155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
blackOrWhite2default:default2
 2default:default2
442default:default2
12default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/blackOrWhite.v2default:default2
232default:default8@Z8-6155h px� 
�
synthesizing module '%s'%s4497*oasys2
PS2Receiver2default:default2
 2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/hdl/PS2Receiver.v2default:default2
232default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2
	debouncer2default:default2
 2default:default2g
QD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/hdl/debouncer.v2default:default2
232default:default8@Z8-6157h px� 
_
%s
*synth2G
3	Parameter COUNT_MAX bound to: 19 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter COUNT_WIDTH bound to: 5 - type: integer 
2default:defaulth p
x
� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
	debouncer2default:default2
 2default:default2
452default:default2
12default:default2g
QD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/hdl/debouncer.v2default:default2
232default:default8@Z8-6155h px� 
�
-case statement is not full and has no default155*oasys2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/hdl/PS2Receiver.v2default:default2
552default:default8@Z8-155h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2
PS2Receiver2default:default2
 2default:default2
462default:default2
12default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/hdl/PS2Receiver.v2default:default2
232default:default8@Z8-6155h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
keycodev2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
932default:default8@Z8-567h px� 
�
synthesizing module '%s'%s4497*oasys2 
quadSevenSeg2default:default2
 2default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
32default:default8@Z8-6157h px� 
�
synthesizing module '%s'%s4497*oasys2!
hexTo7Segment2default:default2
 2default:default2l
VD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/segmentDecoder.v2default:default2
32default:default8@Z8-6157h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2!
hexTo7Segment2default:default2
 2default:default2
472default:default2
12default:default2l
VD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/segmentDecoder.v2default:default2
32default:default8@Z8-6155h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
num02default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
502default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
num12default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
502default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
num22default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
502default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
num32default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
502default:default8@Z8-567h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2 
quadSevenSeg2default:default2
 2default:default2
482default:default2
12default:default2j
TD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/imports/new/quadSevenSeg.v2default:default2
32default:default8@Z8-6155h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
ball_x2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
ball_y2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2

left_bar_y2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
right_bar_y2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
stage2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
restart2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2

left_score2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
right_score2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2
timer2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2 
ball_speed_x2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2 
ball_speed_y2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2!
player1Action2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
8referenced signal '%s' should be on the sensitivity list567*oasys2!
player2Action2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1832default:default8@Z8-567h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2
	start_reg2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1042default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2 
keycodev_reg2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1052default:default8@Z8-6014h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2'
mem_mapped_keyboard2default:default2
 2default:default2
492default:default2
12default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
32default:default8@Z8-6155h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2
ball_x_o2default:default2
102default:default2'
mem_mapped_keyboard2default:default2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
762default:default8@Z8-689h px� 
�
'done synthesizing module '%s'%s (%s#%s)4495*oasys2

z80_tester2default:default2
 2default:default2
502default:default2
12default:default2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-6155h px� 
�
!design %s has unconnected port %s3331*oasys2 
blackOrWhite2default:default2#
scorePlayer1[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2 
blackOrWhite2default:default2#
scorePlayer2[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
sw[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
btnU2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2'
mem_mapped_keyboard2default:default2
btnL2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2 
alu_prep_daa2default:default2
low[0]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[98]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[94]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[93]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[90]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[87]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[71]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[67]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[63]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[62]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[60]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[54]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[41]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[36]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[32]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[22]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[19]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[18]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[14]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[10]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[9]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[8]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[7]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[6]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[5]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[4]2default:defaultZ8-3331h px� 
�
%s*synth2�
xFinished RTL Elaboration : Time (s): cpu = 00:00:20 ; elapsed = 00:00:22 . Memory (MB): peak = 596.254 ; gain = 317.926
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:22 ; elapsed = 00:00:24 . Memory (MB): peak = 596.254 ; gain = 317.926
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:22 ; elapsed = 00:00:24 . Memory (MB): peak = 596.254 ; gain = 317.926
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
Loading part %s157*device2#
xc7a35tcpg236-12default:defaultZ21-403h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
Parsing XDC File [%s]
179*designutils2V
@D:/study/hw/hw-syn-lab-project/Z80.srcs/constrs_1/new/Basys3.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2V
@D:/study/hw/hw-syn-lab-project/Z80.srcs/constrs_1/new/Basys3.xdc2default:default8Z20-178h px� 
�
�Implementation specific constraints were found while reading constraint file [%s]. These constraints will be ignored for synthesis but will be used in implementation. Impacted constraints are listed in the file [%s].
233*project2T
@D:/study/hw/hw-syn-lab-project/Z80.srcs/constrs_1/new/Basys3.xdc2default:default20
.Xil/z80_tester_propImpl.xdc2default:defaultZ1-236h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0082default:default2
872.5122default:default2
0.0002default:defaultZ17-268h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0022default:default2
872.5122default:default2
0.0002default:defaultZ17-268h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0032default:default2
874.7702default:default2
0.0002default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common24
 Constraint Validation Runtime : 2default:default2
00:00:002default:default2 
00:00:00.1552default:default2
875.1092default:default2
2.5982default:defaultZ17-268h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
~Finished Constraint Validation : Time (s): cpu = 00:00:36 ; elapsed = 00:00:47 . Memory (MB): peak = 875.109 ; gain = 596.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7a35tcpg236-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:36 ; elapsed = 00:00:47 . Memory (MB): peak = 875.109 ; gain = 596.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:04:36 ; elapsed = 00:04:50 . Memory (MB): peak = 875.109 ; gain = 596.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
t
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
pla02default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
column2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
score2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
wsStartX2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
score2default:default2
12default:default2
52default:defaultZ8-5544h px� 
q
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
O2default:defaultZ8-5546h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
count2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
cnt2default:default2
42default:default2
52default:defaultZ8-5544h px� 
s
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
mem2default:defaultZ8-5546h px� 
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
ball_x2default:defaultZ8-5546h px� 
v
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
ball_y2default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

left_bar_y2default:defaultZ8-5546h px� 
{
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
right_bar_y2default:defaultZ8-5546h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
stage2default:defaultZ8-5546h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
stage2default:defaultZ8-5546h px� 
w
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
restart2default:defaultZ8-5546h px� 
z
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2

left_score2default:defaultZ8-5546h px� 
{
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
right_score2default:defaultZ8-5546h px� 
u
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2
timer2default:defaultZ8-5546h px� 
|
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2 
ball_speed_x2default:defaultZ8-5546h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-55462default:default2
1002default:defaultZ17-14h px� 
�
!inferring latch for variable '%s'327*oasys2 
wsStartX_reg2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/blackOrWhite.v2default:default2
602default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2 
wsStartY_reg2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/blackOrWhite.v2default:default2
602default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2
	score_reg2default:default2Z
DD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/blackOrWhite.v2default:default2
612default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2%
player1Action_reg2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1112default:default8@Z8-327h px� 
�
!inferring latch for variable '%s'327*oasys2%
player2Action_reg2default:default2i
SD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/imports/new/mem_mapped_keyboard.v2default:default2
1212default:default8@Z8-327h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:04:46 ; elapsed = 00:05:00 . Memory (MB): peak = 875.109 ; gain = 596.781
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 2     
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 41    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 33    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 119   
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	             512K Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  10 Input      9 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      9 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      9 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  14 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 115   
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  10 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      1 Bit        Muxes := 14    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
=
%s
*synth2%
Module clockDiv 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
>
%s
*synth2&
Module clk_delay 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
A
%s
*synth2)
Module decode_state 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 6     
2default:defaulth p
x
� 
<
%s
*synth2$
Module execute 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 2     
2default:defaulth p
x
� 
?
%s
*synth2'
Module interrupts 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 8     
2default:defaulth p
x
� 
7
%s
*synth2
Module ir 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
?
%s
*synth2'
Module pla_decode 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 86    
2default:defaulth p
x
� 
;
%s
*synth2#
Module resets 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 6     
2default:defaulth p
x
� 
?
%s
*synth2'
Module memory_ifc 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 20    
2default:defaulth p
x
� 
>
%s
*synth2&
Module sequencer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 11    
2default:defaulth p
x
� 
@
%s
*synth2(
Module alu_control 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
>
%s
*synth2&
Module alu_flags 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 2     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 10    
2default:defaulth p
x
� 
=
%s
*synth2%
Module alu_core 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
8
%s
*synth2 
Module alu 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit         XORs := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 5     
2default:defaulth p
x
� 
>
%s
*synth2&
Module reg_latch 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
@
%s
*synth2(
Module reg_control 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 4     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
A
%s
*synth2)
Module inc_dec_2bit 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 2     
2default:defaulth p
x
� 
<
%s
*synth2$
Module inc_dec 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
8
%s
*synth2 
+---XORs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit         XORs := 19    
2default:defaulth p
x
� 
B
%s
*synth2*
Module address_latch 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module address_pins 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth p
x
� 
>
%s
*synth2&
Module data_pins 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
E
%s
*synth2-
Module z80_top_direct_n 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
Module vga_sync 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
A
%s
*synth2)
Module whichSegment 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     12 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
A
%s
*synth2)
Module blackOrWhite 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     11 Bit       Adders := 4     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input     12 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  10 Input      9 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      9 Bit        Muxes := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      9 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      9 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit        Muxes := 7     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  10 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   5 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   6 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
>
%s
*synth2&
Module debouncer 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                5 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
@
%s
*synth2(
Module PS2Receiver 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
A
%s
*synth2)
Module quadSevenSeg 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
H
%s
*synth20
Module mem_mapped_keyboard 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               10 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
8
%s
*synth2 
+---RAMs : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	             512K Bit         RAMs := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     12 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  14 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      1 Bit        Muxes := 12    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 15    
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2j
VPart Resources:
DSPs: 90 (col length:60)
BRAMs: 100 (col length: RAMB18 60 RAMB36 30)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2
score2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
+design %s has port %s driven by constant %s3447*oasys2

z80_tester2default:default2
dp2default:default2
12default:defaultZ8-3917h px� 
�
!design %s has unconnected port %s3331*oasys2 
blackOrWhite2default:default2#
scorePlayer1[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2 
blackOrWhite2default:default2#
scorePlayer2[5]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[98]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[94]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[93]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[90]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[87]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[71]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[67]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[63]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[62]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[60]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[54]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[41]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[36]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[32]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[22]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[19]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[18]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[14]2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
execute2default:default2
pla[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[10]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[9]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[8]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[7]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[6]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[5]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
led[4]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[11]2default:defaultZ8-3331h px� 

!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[10]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[9]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[8]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[7]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[6]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[5]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[4]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[3]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[2]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[1]2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
sw[0]2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
btnU2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2

z80_tester2default:default2
btnL2default:defaultZ8-3331h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default23
\cpu/interrupts_/int_armed_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2;
'\cpu/clk_delay_/SYNTHESIZED_WIRE_9_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2;
'\cpu/clk_delay_/SYNTHESIZED_WIRE_8_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2*
mem/right_bar_y_reg[0]2default:default2
FDE2default:default2*
mem/right_bar_y_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2,
\mem/right_bar_y_reg[1] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2)
mem/left_bar_y_reg[0]2default:default2
FDE2default:default2)
mem/left_bar_y_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2+
\mem/left_bar_y_reg[1] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2%
mem/ball_x_reg[0]2default:default2
FDE2default:default2%
mem/ball_x_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2'
\mem/ball_x_reg[1] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2%
mem/ball_y_reg[0]2default:default2
FDE2default:default2%
mem/ball_y_reg[1]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2'
\mem/ball_y_reg[1] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[0]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[4]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[1]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[5]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[2]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[6]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[3]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
mem/uut/dataprev_reg[7]2default:default2
FDE2default:default2*
mem/uut/keycode_reg[7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2,
mem/bOrW/wsStartX_reg[0]2default:default2
LD2default:default2,
mem/bOrW/wsStartX_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2,
mem/bOrW/wsStartX_reg[1]2default:default2
LD2default:default2,
mem/bOrW/wsStartX_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2,
mem/bOrW/wsStartX_reg[2]2default:default2
LD2default:default2,
mem/bOrW/wsStartX_reg[9]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2.
\mem/bOrW/wsStartX_reg[9] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2,
mem/bOrW/wsStartY_reg[0]2default:default2
LD2default:default2,
mem/bOrW/wsStartY_reg[9]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2.
\mem/bOrW/wsStartY_reg[9] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys21
cpu/interrupts_/int_armed_reg2default:default2
FDC2default:default29
%cpu/clk_delay_/SYNTHESIZED_WIRE_7_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2;
'\cpu/clk_delay_/SYNTHESIZED_WIRE_7_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default27
#\cpu/memory_ifc_/DFFE_intr_ff3_reg 2default:defaultZ8-3333h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2,
mem/bOrW/wsStartX_reg[9]2default:default2

z80_tester2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2,
mem/bOrW/wsStartY_reg[9]2default:default2

z80_tester2default:defaultZ8-3332h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:05:53 ; elapsed = 00:06:08 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
2
%s*synth2

ROM:
2default:defaulth px� 
k
%s*synth2S
?+-------------+-------------+---------------+----------------+
2default:defaulth px� 
l
%s*synth2T
@|Module Name  | RTL Object  | Depth x Width | Implemented As | 
2default:defaulth px� 
k
%s*synth2S
?+-------------+-------------+---------------+----------------+
2default:defaulth px� 
l
%s*synth2T
@|pixelSegment | segments    | 32x20         | LUT            | 
2default:defaulth px� 
l
%s*synth2T
@|blackOrWhite | pS/segments | 32x20         | LUT            | 
2default:defaulth px� 
l
%s*synth2T
@+-------------+-------------+---------------+----------------+

2default:defaulth px� 
k
%s*synth2S
?
Distributed RAM: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2i
U+------------+-------------+-----------+----------------------+--------------------+
2default:defaulth px� 
�
%s*synth2j
V|Module Name | RTL Object  | Inference | Size (Depth x Width) | Primitives         | 
2default:defaulth px� 
�
%s*synth2i
U+------------+-------------+-----------+----------------------+--------------------+
2default:defaulth px� 
�
%s*synth2j
V|z80_tester  | mem/mem_reg | Implied   | 64 K x 8             | RAM256X1S x 2048   | 
2default:defaulth px� 
�
%s*synth2j
V+------------+-------------+-----------+----------------------+--------------------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the Distributed RAMs at the current stage of the synthesis flow. Some Distributed RAMs may be reimplemented as non Distributed RAM primitives later in the synthesis flow. Multiple instantiated RAMs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:08:13 ; elapsed = 00:08:30 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_399(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_383(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_393(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_380(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_394(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_381(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_392(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_379(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_397(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_378(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_396(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_377(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_395(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_376(tricell)"
2default:defaulth p
x
� 
j
%s
*synth2R
>Inferred a: "set_disable_timing -from a -to z i_398(tricell)"
2default:defaulth p
x
� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from a -to z sw2_i_382(tricell)"
2default:defaulth p
x
� 
@
%s
*synth2(
      : i_474/db[4]
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
}Finished Timing Optimization : Time (s): cpu = 00:08:19 ; elapsed = 00:08:36 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(
Distributed RAM: Final Mapping  Report
2default:defaulth p
x
� 
�
%s
*synth2i
U+------------+-------------+-----------+----------------------+--------------------+
2default:defaulth p
x
� 
�
%s
*synth2j
V|Module Name | RTL Object  | Inference | Size (Depth x Width) | Primitives         | 
2default:defaulth p
x
� 
�
%s
*synth2i
U+------------+-------------+-----------+----------------------+--------------------+
2default:defaulth p
x
� 
�
%s
*synth2j
V|z80_tester  | mem/mem_reg | Implied   | 64 K x 8             | RAM256X1S x 2048   | 
2default:defaulth p
x
� 
�
%s
*synth2j
V+------------+-------------+-----------+----------------------+--------------------+

2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
|Finished Technology Mapping : Time (s): cpu = 00:08:22 ; elapsed = 00:08:40 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
vFinished IO Insertion : Time (s): cpu = 00:08:24 ; elapsed = 00:08:41 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:08:24 ; elapsed = 00:08:41 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
J
%s
*synth22
     0: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
K
%s
*synth23
     1: flags_xf_i_2/I0 (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     2: flags_xf_i_4/O (LUT2)
2default:defaulth p
x
� 
K
%s
*synth23
     3: flags_xf_i_4/I0 (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     4: flags_xf_i_9/O (LUT6)
2default:defaulth p
x
� 
K
%s
*synth23
     5: flags_xf_i_9/I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[3]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[3]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[3]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[3]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[3]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[3]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
    12: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
i
%s
*synth2Q
=Inferred a: "set_disable_timing -from I0 -to O flags_xf_i_2"
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[3]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[3]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[3]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[3]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[3]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[3]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
     6: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     7: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[3]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[3]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[3]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \latch[3]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \latch[3]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[3]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[3]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[3]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[3]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      8: \latch[3]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     9: \latch[3]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[3]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \latch[3]_i_12 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[3]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[3]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[3]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[3]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[3]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[3]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \latch[3]_i_12 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[3]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[3]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
J
%s
*synth22
     0: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     1: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[3]_i_1 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[3]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[3]_i_2 /O (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[3]_i_2 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
     4: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     5: flags_xf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[3]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
J
%s
*synth22
     0: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
K
%s
*synth23
     1: flags_yf_i_2/I0 (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     2: flags_yf_i_3/O (LUT2)
2default:defaulth p
x
� 
K
%s
*synth23
     3: flags_yf_i_3/I0 (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     4: flags_yf_i_5/O (LUT6)
2default:defaulth p
x
� 
K
%s
*synth23
     5: flags_yf_i_5/I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[5]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[5]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[5]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[5]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[5]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[5]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
    12: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
i
%s
*synth2Q
=Inferred a: "set_disable_timing -from I0 -to O flags_yf_i_2"
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[5]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[5]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[5]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[5]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[5]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[5]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
     6: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     7: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[5]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[5]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[5]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \latch[5]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \latch[5]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[5]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[5]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[5]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[5]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      8: \latch[5]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     9: \latch[5]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[5]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \latch[5]_i_12 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[5]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[5]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[5]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[5]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[5]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[5]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \latch[5]_i_12 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[5]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[5]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
J
%s
*synth22
     0: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     1: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[5]_i_1 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[5]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[5]_i_2 /O (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[5]_i_2 /I2 (LUT4)
2default:defaulth p
x
� 
J
%s
*synth22
     4: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
J
%s
*synth22
     5: flags_yf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[5]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
T
%s
*synth2<
(     0: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     1: DFFE_inst_latch_hf_i_2/I0 (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     2: DFFE_inst_latch_hf_i_5/O (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     3: DFFE_inst_latch_hf_i_5/I0 (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     4: DFFE_inst_latch_hf_i_12/O (LUT6)
2default:defaulth p
x
� 
V
%s
*synth2>
*     5: DFFE_inst_latch_hf_i_12/I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[4]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[4]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[4]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[4]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[4]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[4]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(    12: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
s
%s
*synth2[
GInferred a: "set_disable_timing -from I0 -to O DFFE_inst_latch_hf_i_2"
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[4]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[4]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[4]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[4]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[4]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[4]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(     6: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     7: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[4]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[4]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[4]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \latch[4]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \latch[4]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[4]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[4]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[4]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[4]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      8: \latch[4]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     9: \latch[4]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[4]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \latch[4]_i_12 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[4]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[4]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[4]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[4]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[4]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[4]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \latch[4]_i_12 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[4]_i_12 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[4]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
T
%s
*synth2<
(     0: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     1: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[4]_i_1 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[4]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[4]_i_2 /O (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[4]_i_2 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(     4: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     5: DFFE_inst_latch_hf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[4]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[0]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[0]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[0]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[0]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[0]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \latch[0]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     7: \latch[0]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     8: \latch[0]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     9: \latch[0]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
     10: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[0]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
T
%s
*synth2<
(     0: DFFE_inst_latch_cf_i_2/O (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     1: DFFE_inst_latch_cf_i_2/I0 (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     2: DFFE_inst_latch_cf_i_5/O (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     3: DFFE_inst_latch_cf_i_5/I0 (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     4: DFFE_inst_latch_cf_i_14/O (LUT6)
2default:defaulth p
x
� 
V
%s
*synth2>
*     5: DFFE_inst_latch_cf_i_14/I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[0]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[0]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[0]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[0]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[0]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[0]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(    12: DFFE_inst_latch_cf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
s
%s
*synth2[
GInferred a: "set_disable_timing -from I0 -to O DFFE_inst_latch_cf_i_2"
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[0]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[0]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[0]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[0]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[0]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[0]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(     6: DFFE_inst_latch_cf_i_2/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     7: DFFE_inst_latch_cf_i_2/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[0]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[0]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[0]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \latch[0]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \latch[0]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      5: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \latch[0]_i_12 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[0]_i_1__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[0]_i_1__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[0]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[0]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[0]_i_4__1 /O (LUT6)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[0]_i_4__1 /I4 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      7: \latch[0]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[0]_i_1__1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
T
%s
*synth2<
(     0: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     1: DFFE_inst_latch_sf_i_3/I0 (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     2: DFFE_inst_latch_sf_i_11/O (LUT2)
2default:defaulth p
x
� 
V
%s
*synth2>
*     3: DFFE_inst_latch_sf_i_11/I0 (LUT2)
2default:defaulth p
x
� 
U
%s
*synth2=
)     4: DFFE_inst_latch_sf_i_21/O (LUT6)
2default:defaulth p
x
� 
V
%s
*synth2>
*     5: DFFE_inst_latch_sf_i_21/I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[7]_i_2__2 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[7]_i_2__2 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      8: \latch[7]_i_4 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     9: \latch[7]_i_4 /I4 (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$    10: \latch[7]_i_11__0 /O (LUT4)
2default:defaulth p
x
� 
Q
%s
*synth29
%    11: \latch[7]_i_11__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(    12: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
s
%s
*synth2[
GInferred a: "set_disable_timing -from I0 -to O DFFE_inst_latch_sf_i_3"
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[7]_i_2__2 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[7]_i_2__2 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[7]_i_4 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[7]_i_4 /I4 (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     4: \latch[7]_i_11__0 /O (LUT4)
2default:defaulth p
x
� 
Q
%s
*synth29
%     5: \latch[7]_i_11__0 /I2 (LUT4)
2default:defaulth p
x
� 
T
%s
*synth2<
(     6: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     7: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[7]_i_2__2 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
N
%s
*synth26
"     0: \latch[7]_i_105 /O (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     1: \latch[7]_i_105 /I0 (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     2: \latch[7]_i_184 /O (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     3: \latch[7]_i_184 /I2 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[7]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[7]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[7]_i_7 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[7]_i_7 /I4 (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     8: \latch[7]_i_30 /O (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     9: \latch[7]_i_30 /I2 (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"    10: \latch[7]_i_105 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
m
%s
*synth2U
AInferred a: "set_disable_timing -from I0 -to O \latch[7]_i_105 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[7]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[7]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[7]_i_7 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[7]_i_7 /I4 (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     4: \latch[7]_i_30 /O (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     5: \latch[7]_i_30 /I2 (LUT4)
2default:defaulth p
x
� 
N
%s
*synth26
"     6: \latch[7]_i_105 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     7: \latch[7]_i_105 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[7]_i_2__1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
T
%s
*synth2<
(     0: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
T
%s
*synth2<
(     1: DFFE_inst_latch_sf_i_3/O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[2]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[2]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[2]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[2]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[2]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[2]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[2]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[2]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     8: \latch[2]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     9: \latch[2]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[2]_i_13 /O (LUT6)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I2 -to O \latch[2]_i_13 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[2]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[2]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[2]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[2]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[2]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[2]_i_8 /I0 (LUT4)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[2]_i_1__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[2]_i_1__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[2]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[2]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[2]_i_4__1 /O (LUT6)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[2]_i_4__1 /I4 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
     12: \latch[2]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[2]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[2]_i_4 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[2]_i_4 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[2]_i_5 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[2]_i_5 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     4: \opcode[2]_i_7 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     5: \opcode[2]_i_7 /I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[2]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[2]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[2]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[2]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[2]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[2]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    12: \opcode[2]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[2]_i_4 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[2]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[2]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[2]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[2]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[2]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[2]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \opcode[2]_i_4 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \opcode[2]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[2]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[1]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[1]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[1]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[1]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[1]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[1]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[1]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[1]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     8: \latch[1]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     9: \latch[1]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[1]_i_13 /O (LUT6)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I2 -to O \latch[1]_i_13 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[1]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[1]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[1]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[1]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[1]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[1]_i_8 /I0 (LUT4)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[1]_i_1__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[1]_i_1__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[1]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[1]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[1]_i_4__1 /O (LUT6)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[1]_i_4__1 /I4 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
     12: \latch[1]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[1]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[1]_i_4 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[1]_i_4 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[1]_i_5 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[1]_i_5 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     4: \opcode[1]_i_7 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     5: \opcode[1]_i_7 /I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[1]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[1]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[1]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[1]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[1]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[1]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    12: \opcode[1]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[1]_i_4 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[1]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[1]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[1]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[1]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[1]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[1]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \opcode[1]_i_4 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \opcode[1]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[1]_i_1__0 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \latch[6]_i_13 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \latch[6]_i_13 /I2 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[6]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[6]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[6]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[6]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      6: \latch[6]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \latch[6]_i_8 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     8: \latch[6]_i_12 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     9: \latch[6]_i_12 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!    10: \latch[6]_i_13 /O (LUT6)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I2 -to O \latch[6]_i_13 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
L
%s
*synth24
      0: \latch[6]_i_1 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     1: \latch[6]_i_1 /I0 (LUT2)
2default:defaulth p
x
� 
L
%s
*synth24
      2: \latch[6]_i_2 /O (LUT5)
2default:defaulth p
x
� 
M
%s
*synth25
!     3: \latch[6]_i_2 /I4 (LUT5)
2default:defaulth p
x
� 
L
%s
*synth24
      4: \latch[6]_i_8 /O (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     5: \latch[6]_i_8 /I0 (LUT4)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[6]_i_1__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[6]_i_1__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[6]_i_2__1 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[6]_i_2__1 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[6]_i_4__1 /O (LUT6)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[6]_i_4__1 /I4 (LUT6)
2default:defaulth p
x
� 
L
%s
*synth24
     12: \latch[6]_i_1 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
k
%s
*synth2S
?Inferred a: "set_disable_timing -from I0 -to O \latch[6]_i_1 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
M
%s
*synth25
!     0: \opcode[6]_i_4 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     1: \opcode[6]_i_4 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     2: \opcode[6]_i_5 /O (LUT2)
2default:defaulth p
x
� 
N
%s
*synth26
"     3: \opcode[6]_i_5 /I0 (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     4: \opcode[6]_i_7 /O (LUT6)
2default:defaulth p
x
� 
N
%s
*synth26
"     5: \opcode[6]_i_7 /I4 (LUT6)
2default:defaulth p
x
� 
O
%s
*synth27
#     6: \latch[6]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     7: \latch[6]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     8: \latch[6]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     9: \latch[6]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#    10: \latch[6]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$    11: \latch[6]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!    12: \opcode[6]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
l
%s
*synth2T
@Inferred a: "set_disable_timing -from I0 -to O \opcode[6]_i_4 "
2default:defaulth p
x
� 
?
%s
*synth2'
Found timing loop:
2default:defaulth p
x
� 
O
%s
*synth27
#     0: \latch[6]_i_1__0 /O (LUT2)
2default:defaulth p
x
� 
P
%s
*synth28
$     1: \latch[6]_i_1__0 /I0 (LUT2)
2default:defaulth p
x
� 
O
%s
*synth27
#     2: \latch[6]_i_2__0 /O (LUT5)
2default:defaulth p
x
� 
P
%s
*synth28
$     3: \latch[6]_i_2__0 /I4 (LUT5)
2default:defaulth p
x
� 
O
%s
*synth27
#     4: \latch[6]_i_8__0 /O (LUT4)
2default:defaulth p
x
� 
P
%s
*synth28
$     5: \latch[6]_i_8__0 /I2 (LUT4)
2default:defaulth p
x
� 
M
%s
*synth25
!     6: \opcode[6]_i_4 /O (LUT2)
2default:defaulth p
x
� 
M
%s
*synth25
!     7: \opcode[6]_i_4 /O (LUT2)
2default:defaulth p
x
� 
�
found timing loop.295*oasys2X
BD:/study/hw/hw-syn-lab-project/Z80.srcs/sources_1/new/z80_tester.v2default:default2
232default:default8@Z8-295h px� 
n
%s
*synth2V
BInferred a: "set_disable_timing -from I0 -to O \latch[6]_i_1__0 "
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
G
%s*synth2/
|      |Cell      |Count |
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
G
%s*synth2/
|1     |BUFG      |     3|
2default:defaulth px� 
G
%s*synth2/
|2     |CARRY4    |    44|
2default:defaulth px� 
G
%s*synth2/
|3     |LUT1      |    45|
2default:defaulth px� 
G
%s*synth2/
|4     |LUT2      |   405|
2default:defaulth px� 
G
%s*synth2/
|5     |LUT3      |   325|
2default:defaulth px� 
G
%s*synth2/
|6     |LUT4      |   436|
2default:defaulth px� 
G
%s*synth2/
|7     |LUT5      |   416|
2default:defaulth px� 
G
%s*synth2/
|8     |LUT6      |  2014|
2default:defaulth px� 
G
%s*synth2/
|9     |MUXF7     |   276|
2default:defaulth px� 
G
%s*synth2/
|10    |MUXF8     |   134|
2default:defaulth px� 
G
%s*synth2/
|11    |RAM256X1S |  2048|
2default:defaulth px� 
G
%s*synth2/
|12    |FDCE      |    81|
2default:defaulth px� 
G
%s*synth2/
|13    |FDPE      |     5|
2default:defaulth px� 
G
%s*synth2/
|14    |FDRE      |   415|
2default:defaulth px� 
G
%s*synth2/
|15    |LD        |    19|
2default:defaulth px� 
G
%s*synth2/
|16    |LDC       |     2|
2default:defaulth px� 
G
%s*synth2/
|17    |LDCP      |     2|
2default:defaulth px� 
G
%s*synth2/
|18    |IBUF      |     4|
2default:defaulth px� 
G
%s*synth2/
|19    |OBUF      |    30|
2default:defaulth px� 
G
%s*synth2/
|20    |OBUFT     |    12|
2default:defaulth px� 
G
%s*synth2/
+------+----------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
i
%s
*synth2Q
=+------+-----------------------+--------------------+------+
2default:defaulth p
x
� 
i
%s
*synth2Q
=|      |Instance               |Module              |Cells |
2default:defaulth p
x
� 
i
%s
*synth2Q
=+------+-----------------------+--------------------+------+
2default:defaulth p
x
� 
i
%s
*synth2Q
=|1     |top                    |                    |  6716|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|2     |  cpu                  |z80_top_direct_n    |  2865|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|3     |    address_latch_     |address_latch       |    42|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|4     |    address_pins_      |address_pins        |   410|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|5     |    alu_               |alu                 |    43|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|6     |    alu_control_       |alu_control         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|7     |    alu_flags_         |alu_flags           |    15|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|8     |    data_pins_         |data_pins           |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|9     |    decode_state_      |decode_state        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|10    |    execute_           |execute             |  1253|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|11    |    interrupts_        |interrupts          |     1|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|12    |    ir_                |ir                  |   153|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|13    |    memory_ifc_        |memory_ifc          |    43|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|14    |    reg_control_       |reg_control         |     7|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|15    |    reg_file_          |reg_file            |   335|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|16    |      b2v_latch_af2_hi |reg_latch           |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|17    |      b2v_latch_af2_lo |reg_latch_32        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|18    |      b2v_latch_af_hi  |reg_latch_33        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|19    |      b2v_latch_af_lo  |reg_latch_34        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|20    |      b2v_latch_bc2_hi |reg_latch_35        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|21    |      b2v_latch_bc2_lo |reg_latch_36        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|22    |      b2v_latch_bc_hi  |reg_latch_37        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|23    |      b2v_latch_bc_lo  |reg_latch_38        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|24    |      b2v_latch_de2_hi |reg_latch_39        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|25    |      b2v_latch_de2_lo |reg_latch_40        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|26    |      b2v_latch_de_hi  |reg_latch_41        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|27    |      b2v_latch_de_lo  |reg_latch_42        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|28    |      b2v_latch_hl2_hi |reg_latch_43        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|29    |      b2v_latch_hl2_lo |reg_latch_44        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|30    |      b2v_latch_hl_hi  |reg_latch_45        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|31    |      b2v_latch_hl_lo  |reg_latch_46        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|32    |      b2v_latch_ir_hi  |reg_latch_47        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|33    |      b2v_latch_ir_lo  |reg_latch_48        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|34    |      b2v_latch_ix_hi  |reg_latch_49        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|35    |      b2v_latch_ix_lo  |reg_latch_50        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|36    |      b2v_latch_iy_hi  |reg_latch_51        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|37    |      b2v_latch_iy_lo  |reg_latch_52        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|38    |      b2v_latch_pc_hi  |reg_latch_53        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|39    |      b2v_latch_pc_lo  |reg_latch_54        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|40    |      b2v_latch_sp_hi  |reg_latch_55        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|41    |      b2v_latch_sp_lo  |reg_latch_56        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|42    |      b2v_latch_wz_hi  |reg_latch_57        |    32|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|43    |      b2v_latch_wz_lo  |reg_latch_58        |    31|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|44    |    resets_            |resets              |    25|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|45    |    sequencer_         |sequencer           |   511|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|46    |  fdivTarget           |clockDiv            |     1|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|47    |  \genblk1[0].fdiv     |clockDiv_0          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|48    |  \genblk1[10].fdiv    |clockDiv_1          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|49    |  \genblk1[11].fdiv    |clockDiv_2          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|50    |  \genblk1[1].fdiv     |clockDiv_3          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|51    |  \genblk1[2].fdiv     |clockDiv_4          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|52    |  \genblk1[3].fdiv     |clockDiv_5          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|53    |  \genblk1[4].fdiv     |clockDiv_6          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|54    |  \genblk1[5].fdiv     |clockDiv_7          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|55    |  \genblk1[6].fdiv     |clockDiv_8          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|56    |  \genblk1[7].fdiv     |clockDiv_9          |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|57    |  \genblk1[8].fdiv     |clockDiv_10         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|58    |  \genblk1[9].fdiv     |clockDiv_11         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|59    |  mem                  |mem_mapped_keyboard |  3776|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|60    |    bOrW               |blackOrWhite        |   193|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|61    |      ws               |whichSegment        |   108|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|62    |    fdivTarget         |clockDiv_12         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|63    |    \genblk1[0].fdiv   |clockDiv_13         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|64    |    \genblk1[10].fdiv  |clockDiv_14         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|65    |    \genblk1[11].fdiv  |clockDiv_15         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|66    |    \genblk1[12].fdiv  |clockDiv_16         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|67    |    \genblk1[13].fdiv  |clockDiv_17         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|68    |    \genblk1[14].fdiv  |clockDiv_18         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|69    |    \genblk1[15].fdiv  |clockDiv_19         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|70    |    \genblk1[16].fdiv  |clockDiv_20         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|71    |    \genblk1[17].fdiv  |clockDiv_21         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|72    |    \genblk1[1].fdiv   |clockDiv_22         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|73    |    \genblk1[2].fdiv   |clockDiv_23         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|74    |    \genblk1[3].fdiv   |clockDiv_24         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|75    |    \genblk1[4].fdiv   |clockDiv_25         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|76    |    \genblk1[5].fdiv   |clockDiv_26         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|77    |    \genblk1[6].fdiv   |clockDiv_27         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|78    |    \genblk1[7].fdiv   |clockDiv_28         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|79    |    \genblk1[8].fdiv   |clockDiv_29         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|80    |    \genblk1[9].fdiv   |clockDiv_30         |     2|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|81    |    q7seg              |quadSevenSeg        |     8|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|82    |    uut                |PS2Receiver         |   109|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|83    |      db_clk           |debouncer           |    15|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|84    |      db_data          |debouncer_31        |    16|
2default:defaulth p
x
� 
i
%s
*synth2Q
=|85    |    vga_sync_unit      |vga_sync            |   356|
2default:defaulth p
x
� 
i
%s
*synth2Q
=+------+-----------------------+--------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 40 critical warnings and 55 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
Synthesis Optimization Runtime : Time (s): cpu = 00:07:52 ; elapsed = 00:08:22 . Memory (MB): peak = 1033.441 ; gain = 476.258
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:08:25 ; elapsed = 00:08:42 . Memory (MB): peak = 1033.441 ; gain = 755.113
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
h
-Analyzing %s Unisim elements for replacement
17*netlist2
25252default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
22default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0192default:default2
1033.4412default:default2
0.0002default:defaultZ17-268h px� 
�
!Unisim Transformation Summary:
%s111*project2�
�  A total of 2071 instances were transformed.
  LD => LDCE: 19 instances
  LDC => LDCE: 2 instances
  LDCP => LDCP (GND, LUT3, LUT3, LDCE, VCC): 2 instances
  RAM256X1S => RAM256X1S (MUXF7, MUXF7, MUXF8, RAMS64E, RAMS64E, RAMS64E, RAMS64E): 2048 instances
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
2522default:default2
1302default:default2
402default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2"
synth_design: 2default:default2
00:08:322default:default2
00:08:502default:default2
1033.4412default:default2
766.8592default:defaultZ17-268h px� 
�
I%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s
268*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2 
00:00:00.0142default:default2
1033.4412default:default2
0.0002default:defaultZ17-268h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2R
>D:/study/hw/hw-syn-lab-project/Z80.runs/synth_1/z80_tester.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2~
jExecuting : report_utilization -file z80_tester_utilization_synth.rpt -pb z80_tester_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Tue May 21 10:48:00 20192default:defaultZ17-206h px� 


End Record